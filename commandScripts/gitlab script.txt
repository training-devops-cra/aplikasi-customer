Git global setup
git config --global user.name "maegan"
git config --global user.email "maegan.c27@hcoupledp.com"

Create a new repository
git clone https://gitlab.com/training-devops-cra/aplikasi-customer.git
cd aplikasi-customer
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/training-devops-cra/aplikasi-customer.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/training-devops-cra/aplikasi-customer.git
git push -u origin --all
git push -u origin --tags