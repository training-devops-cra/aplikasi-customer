package com.devops.training.aplikasicustomer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devops.training.aplikasicustomer.dao.CustomerDao;
import com.devops.training.aplikasicustomer.entity.Customer;

@RestController
public class CustomerController {

	@Autowired private CustomerDao customerDao;
	
	@GetMapping("/customer/")
	public Page<Customer> lihatDataCustosmer(Pageable page){
		return customerDao.findAll(page);
	}
	
	@GetMapping("/customer/{id}")
    public Customer cariById(@PathVariable(name = "id") Customer c){
        return c;
    }
    
    @PostMapping("/insertcustomer/")
    public void simpanDataCustomer(@RequestBody @Valid Customer c){
    	System.out.println(c.getFullname());
    	System.out.println(c.getEmail());
        customerDao.save(c);
    }

}
