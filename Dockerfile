FROM openjdk:8-jdk-alpine
COPY target/*.jar /var/lib/aplikasi-customer/aplikasi-customer.jar
RUN sh -c 'touch /var/lib/aplikasi-customer/aplikasi-customer.jar'
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/var/lib/aplikasi-customer/aplikasi-customer.jar"]